﻿using System;
using System.Runtime.InteropServices;
using System.ComponentModel;
using Microsoft.Office.Interop.Outlook;
using Microsoft.Win32;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace DCOutlookSpam
{
    /// <summary>
    ///   Add-in Express Add-in Module
    /// </summary>
    [GuidAttribute("75B64EDB-3A56-4891-92B6-0EF908E6E3FC"), ProgId("DCOutlookSpam.AddinModule")]
    public class AddinModule : AddinExpress.MSO.ADXAddinModule
    {
        public AddinModule()
        {
            InitializeComponent();
        }

        private AddinExpress.MSO.ADXOLSolutionModule adxolSolutionModule1;
 
        #region Component Designer generated code
        /// <summary>
        /// Required by designer
        /// </summary>
        private System.ComponentModel.IContainer components;
 
        /// <summary>
        /// Required by designer support - do not modify
        /// the following method
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.adxolSolutionModule1 = new AddinExpress.MSO.ADXOLSolutionModule(this.components);
            // 
            // AddinModule
            // 
            this.AddinName = "DCOutlookSpam";
            this.SupportedApps = AddinExpress.MSO.ADXOfficeHostApp.ohaOutlook;
            this.AddinStartupComplete += new AddinExpress.MSO.ADXEvents_EventHandler(this.AddinModule_AddinStartupComplete);

        }
        #endregion
 
        #region Add-in Express automatic code
 
        // Required by Add-in Express - do not modify
        // the methods within this region
 
        public override System.ComponentModel.IContainer GetContainer()
        {
            if (components == null)
                components = new System.ComponentModel.Container();
            return components;
        }
 
        [ComRegisterFunctionAttribute]
        public static void AddinRegister(Type t)
        {
            AddinExpress.MSO.ADXAddinModule.ADXRegister(t);
        }
 
        [ComUnregisterFunctionAttribute]
        public static void AddinUnregister(Type t)
        {
            AddinExpress.MSO.ADXAddinModule.ADXUnregister(t);
        }
 
        public override void UninstallControls()
        {
            base.UninstallControls();
        }

        #endregion

        public static new AddinModule CurrentInstance
        {
            get
            {
                return AddinExpress.MSO.ADXAddinModule.CurrentInstance as AddinModule;
            }
        }

        public Outlook._Application OutlookApp
        {
            get
            {
                return (HostApplication as Outlook._Application);
            }
        }

        private void AddinModule_AddinStartupComplete(object sender, EventArgs e)
        {
            // Find Suspicious folder
            NameSpace ns = OutlookApp.GetNamespace("Mapi");
            var nsFolders = ns.Folders;

            for (int i = 1; i <= nsFolders.Count; i++)
            {
                MAPIFolder personalFolders = nsFolders[i];

                Folders subFolders = personalFolders.Folders;
                foreach (Folder folder in subFolders)
                {
                    if (folder.Name == "DCSpam")
                    {
                        _suspiciousFolder = folder;
                        break;
                    }

                    Release(folder);
                }

                Release(subFolders);
                Release(personalFolders);
            }

            Release(nsFolders);
            Release(ns);


            if (_suspiciousFolder != null)
            {
                // Read connection string to registry
                string spamExtensions = (string) Registry.CurrentUser.CreateSubKey(@"Software\DCSoft\DCOutlookSpam", RegistryKeyPermissionCheck.ReadSubTree)
                                                .GetValue(@"SpamExts", @".info");

                // Get notified when events occur in the Inbox (e.g. mail is received)
                _inboxEvents = new InboxItemsEventsClass(this, _suspiciousFolder, spamExtensions);
                _inboxEvents.ConnectTo(AddinExpress.MSO.ADXOlDefaultFolders.olFolderInbox, true);

                // Get notified when events occur in Sent Folder (e.g. new mail is sent)
                _sentFolderEvents = new SentItemsEventsClass(this, OutlookApp);
                _sentFolderEvents.ConnectTo(AddinExpress.MSO.ADXOlDefaultFolders.olFolderSentMail, true);

                
            }
        }


        private static void Release(object obj)
        {
            if (obj != null)
                Marshal.ReleaseComObject(obj);
        }

        
        InboxItemsEventsClass   _inboxEvents;
        SentItemsEventsClass    _sentFolderEvents;
        Folder                  _suspiciousFolder;
    }
}

