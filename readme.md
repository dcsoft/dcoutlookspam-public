# DC Outlook Spam

## Problem
My e-mail addresss has not changed since 1995, and has hundreds of spam delivered to it.  I only want to receive "important" e-mails on my mobile phone.  I did not constantly want to manage who was or was not "important".  So I developed this Microsoft Outlook add-in to selectively forward received e-mails to my mobile phone, but only from recipients whom I have directly e-mailed.  This takes care of most e-mails that are truly "important", in a seemless way.

I also wanted the ability to reply to the person from my mobile phone.  Therefore, simply forwarding the e-mail using the standard method was not enough, because then the sender would be myself and not the original person who created the e-mail.  This Outlook code sets the sender back to the original person to enable this.  

## Seondary Problem
I wanted to identify spam based on whether the sender contains "info@" or ".info" or the subject contains "SPAM" (meaning the server's junk filter had flagged it).  The simple Outlook Rules don't allow this kind of flexibility, so I wrote my own spam filter using this add-in.


## Dependencies
This add-in uses the excellent commercial [Add-In Express](https://www.add-in-express.com/).  Due to redistribution restrictions, this project is therefore not buildable.

## Notes
DC 1/2018:  The development is being done in the [ERG2 HYPERV VM] in VS 2010.