﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using AddinExpress.MSO;
using Microsoft.Office.Interop.Outlook;

namespace DCOutlookSpam
{
    /// <summary>
    /// Add-in Express Outlook Items Events Class
    /// </summary>
    public class SentItemsEventsClass : AddinExpress.MSO.ADXOutlookItemsEvents
    {
        public SentItemsEventsClass(AddinExpress.MSO.ADXAddinModule module)
            : base(module)
        {
        }

        public SentItemsEventsClass(AddinModule module, _Application outlookApp)
            : base(module)
        {
            s_outlookApp = outlookApp;
        }

        private static bool BuildSentEmailAddresses()
        {
            // Abort if file is already built
            if (File.Exists(SentEmailAddressesFilePath(true)))
                return false;

            // Find Sent Items folder
            Folder sentMailFolder = null;
            NameSpace ns = s_outlookApp.GetNamespace("Mapi");
            var nsFolders = ns.Folders;
            if (nsFolders.Count == 1)     // Personal Folders
            {
                MAPIFolder personalFolders = nsFolders[1];
                Folders subFolders = personalFolders.Folders;
                foreach (Folder folder in subFolders)
                {
                    if (folder.Name == "Sent Items")
                    {
                        sentMailFolder = folder;
                        break;
                    }

                    Release(folder);
                }

                Release(subFolders);
                Release(personalFolders);
            }

            Release(nsFolders);
            Release(ns);

            if (sentMailFolder == null)
                return false;

            try
            {
                // Process e-mails received within 1 year
                _sentEmailAddresssesDict.Clear();
                StringBuilder b = new StringBuilder();
                DateTime startInterestingPeriod = DateTime.Now.AddYears(-1);
                var items = sentMailFolder.Items;
                foreach (object objItem in items)
                {
                    MailItem mailItem = objItem as MailItem;
                    if (mailItem != null && mailItem.ReceivedTime.Year < 4501 && mailItem.ReceivedTime >= startInterestingPeriod) // 4501 is a weird date that shows up
                    {
                        foreach (Recipient r in mailItem.Recipients)
                        {
                            string address = r.Address.ToLower();
                            if (address.Contains("@")) // it's a POP address
                            {
                                if (!_sentEmailAddresssesDict.ContainsKey(address)) // avoid duplicates
                                {
                                    _sentEmailAddresssesDict.Add(address, 0);
                                    b.AppendLine(address);
                                }
                            }
                        }
                    }

                    Release(objItem);
                }
                Release(items);

                SentEmailAddresses = b.ToString();
            }
            finally
            {
                Release(sentMailFolder);
            }

            return true;
        }

        private static void LoadSentEmailAddresses()
        {
            SentEmailAddresses = File.ReadAllText(SentEmailAddressesFilePath(true));
        }

        private static void SaveSentEmailAddresses()
        {
            // Create file
            Directory.CreateDirectory(SentEmailAddressesFilePath(false));
            using (StreamWriter sw = File.CreateText(SentEmailAddressesFilePath(true)))
            {
                sw.Write(SentEmailAddresses);
            }
        }



        public override void ProcessItemAdd(object item)
        {
            // Abort if it is not a MailItem
            var mailItem = item as MailItem;
            if (mailItem == null)
                return;

            // Don't add the item to Sent Folders if it was sent to "dc_iphone@dcsoft.com"
            string toAddress = mailItem.To.ToLower();
            if (toAddress == "'dc_iphone@dcsoft.com'")
            {
                mailItem.Delete();
                return;
            }

            // Add Recipients' e-mail address to list used to forward interesting e-mails
            foreach (Recipient r in mailItem.Recipients)
            {
                string address = r.Address.ToLower();
                if (address.Contains("@")) // it's a POP address
                    if (!SentEmailAddresses.Contains(address))
                        SentEmailAddresses += "\n" + address;
            }
        }


        public override void ProcessItemChange(object item)
        {
            // TODO: Add some code
        }

        public override void ProcessItemRemove()
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeFolderMove(object moveTo, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        public override void ProcessBeforeItemMove(object item, object moveTo, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }

        private static void Release(object obj)
        {
            if (obj != null)
                Marshal.ReleaseComObject(obj);
        }

        static private Dictionary<string, int> _sentEmailAddresssesDict = new Dictionary<string, int>();
        static private string _sentEmailAddresses;
        static public string SentEmailAddresses
        {
            get
            {
                if (_sentEmailAddresses == null)
                {
                    bool built = BuildSentEmailAddresses();

                    LoadSentEmailAddresses();

                    if (built)
                        SaveSentEmailAddresses();
                }

                return _sentEmailAddresses;
            }

            set
            {
                _sentEmailAddresses = value;

                SaveSentEmailAddresses();
            }
        }

        static private string SentEmailAddressesFilePath(bool includeFilename)
        {
            string folder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\DCSoft\DCOutlook";
            if (!includeFilename)
                return folder;

            folder += @"\SentEmailAddresses.txt";

            return folder;
        }


        private static _Application s_outlookApp;
    }
}

