﻿using System;
using System.Runtime.InteropServices;
using AddinExpress.MSO;
using Microsoft.Office.Interop.Outlook;

namespace DCOutlookSpam
{
    /// <summary>
    /// Add-in Express Outlook Items Events Class
    /// </summary>
    public class InboxItemsEventsClass : AddinExpress.MSO.ADXOutlookItemsEvents
    {
        public InboxItemsEventsClass(AddinExpress.MSO.ADXAddinModule module): base(module)
        {
        }

        public InboxItemsEventsClass(ADXAddinModule module, Folder suspiciousFolder, string spamExtensions)
            : base(module)
        {
            _suspiciousFolder = suspiciousFolder;
            _spamExtensions = spamExtensions;
        }

        public override void ProcessItemAdd(object item)
        {
            // Abort if no place to move item to
            if ( _suspiciousFolder == null )
                return;

            // Abort if it is not a MailItem
            var mailItem = item as MailItem;
            if (mailItem == null)
                return;

            try
            {
                // Forward the mailItem if it is sent by someone we have "recently" sent e-mail to
                if ((mailItem.SenderEmailAddress != null) &&
                    mailItem.SenderEmailAddress.Contains("@") && mailItem.SenderEmailAddress.Contains(".") &&
                    SentItemsEventsClass.SentEmailAddresses.Contains(mailItem.SenderEmailAddress.ToLower()))
                {
                    ForwardMailItem(mailItem);
                }
                else
                {
                    // Run DCSpam filter
                    JunkMailItem(mailItem);
                }
            }
            catch (COMException ex)
            {
                // Getting "MailItem does not exist exceptions" when copying and pasting an e-mail message
            }
        }

        private void JunkMailItem(MailItem mailItem)
        {
            bool junk = false;

            // Examine sender e-mail to junk it
            if (mailItem.SenderEmailAddress != null)
            {
                // If sender e-mail address starts with "info@" then junk it
                if (mailItem.SenderEmailAddress.StartsWith("info@", StringComparison.CurrentCultureIgnoreCase))
                {
                    junk = true;
                }
                else
                {
                    int lastDotIndex = mailItem.SenderEmailAddress.LastIndexOf('.');
                    if (lastDotIndex < 0)
                        return;

                    // If sender e-mail ends in a user-defined extension (e.g. ".info"), junk it
                    string mailExt = mailItem.SenderEmailAddress.Substring(lastDotIndex);
                    string[] exts = _spamExtensions.Split(';');
                    for (int i = 0; i < exts.GetLength(0); i++)
                    {
                        if (string.Compare(exts[i], mailExt, StringComparison.CurrentCultureIgnoreCase) == 0)
                        {
                            junk = true;
                            break;
                        }
                    }
                }
            }

            // If subject starts with '[SPAM' junk it
            junk = junk || ((mailItem.Subject != null) && (mailItem.Subject.StartsWith("[SPAM")));

            if (junk)
            {
                mailItem.UnRead = false;
                mailItem.Move(_suspiciousFolder);
            }
        }

        private void ForwardMailItem(MailItem mailItem)
        {
#if false
            // DC 10/27/2015:  Attempt to send via SMTP-specific MailMessage since it has ReplyToList, but got "SMTP server not configured"
            // e.g. Tyler Martin <tyler@erginc.com>
            string from = string.Format("{0} <{1}>", mailItem.SenderName, mailItem.SenderEmailAddress);
            string to = "dc_iphone@dcsoft.com";
            string subject = mailItem.Subject;
            string body = mailItem.Body;
            MailMessage msg = new MailMessage(from, to, subject, body);
            foreach (Recipient r in mailItem.Recipients)
                msg.ReplyToList.Add(new MailAddress(r.Address, r.Name));

            try
            {
                new SmtpClient().Send(msg);
            }
            catch(System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
#endif

            MailItem forwardItem = (MailItem) mailItem.Copy();

            // DC 10/27/2015:  Attempt to add to Reply-To, no dice
            //foreach (Recipient r in forwardItem.Recipients)
                //forwardItem.ReplyRecipients.Add(r.Address);

            forwardItem.To = "dc_iphone@dcsoft.com";

            // Remove any CC and BCC so that we are the only one that gets the forwarded message
            if (!string.IsNullOrEmpty(forwardItem.CC))
                forwardItem.CC = string.Empty;

            if (!string.IsNullOrEmpty(forwardItem.BCC))
                forwardItem.BCC = string.Empty;


            forwardItem.Send();

            Release(forwardItem);
        }

        public override void ProcessItemChange(object item)
        {
            // TODO: Add some code
        }
 
        public override void ProcessItemRemove()
        {
            // TODO: Add some code
        }
 
        public override void ProcessBeforeFolderMove(object moveTo, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }
 
        public override void ProcessBeforeItemMove(object item, object moveTo, AddinExpress.MSO.ADXCancelEventArgs e)
        {
            // TODO: Add some code
        }


        private static void Release(object obj)
        {
            if (obj != null)
                Marshal.ReleaseComObject(obj);
        }


        private readonly Folder _suspiciousFolder;
        private readonly string _spamExtensions;        // e.g. ".info;.ru;.no, etc.
    }
}

